#!/bin/sh -e

# this scripe file path.
cd `echo ${0%/*}`
THIS_FILE_PATH=`pwd` 
#THIS_FILE_PATH=$THIS_FILE_PATH/`basename $0`
THIS_FILE_PATH=$THIS_FILE_PATH/
#echo $THIS_FILE_PATH

mkdir -p ../../build

gcc -O3 ../../applets/compress/src/main.c -D_LARGEFILE64_SOURCE=1 -DHAVE_HIDDEN -o ../../build/main -I../../include/zlib/ -L../../lib/x86_64-linux-x86_64-linux-gcc-4.8-release/zlib-1.2.9/ -lz

exit 0


