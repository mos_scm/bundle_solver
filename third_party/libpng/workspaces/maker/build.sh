#!/bin/sh -e

# this scripe file path.
cd `echo ${0%/*}`
THIS_FILE_PATH=`pwd` 
#THIS_FILE_PATH=$THIS_FILE_PATH/`basename $0`
THIS_FILE_PATH=$THIS_FILE_PATH/
#echo $THIS_FILE_PATH

mkdir -p ../../build

g++ -O3 ../../applets/lsusb/src/main.cpp -o ../../build/lsusb -I../../include -L../../lib/x86_64-linux-x86_64-linux-gcc-4.8-release/libusb-1.0.19/ -lusb-1.0 -pthread

exit 0


